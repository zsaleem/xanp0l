import React from 'react';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import Grid from '@mui/material/Grid';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import { red } from '@mui/material/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import useMediaQuery from '@mui/material/useMediaQuery';
import Link from '@mui/material/Link';
import { Redirect, useHistory } from 'react-router-dom';
import { useStyles } from './styled';

const SearchItems: React.FC<any> = ({ items }) => {
	const medium = useMediaQuery('(max-width:900px)');
	const small = useMediaQuery('(max-width:750px)');
	const classes = useStyles();
	const history = useHistory();

	const getRepoDetail = (event: any, repo: any) => {
		event.preventDefault();
		history.push(`/detail/${repo.owner.login}/${repo.name}`);
	}
	
	return (
		<>
			{
				items?.map((repo: any, index: number) => (
					<Grid item xs={small ? 12 : medium ? 4 : 3} key={`${repo.id}-${index}`}>
						<Card
							sx={{ maxWidth: 345 }}
							className={small ? classes.responsive : classes.normal}>
							<CardHeader
								action={
									<IconButton aria-label='settings'>
										<MoreVertIcon />
									</IconButton>
								}
								title={repo.name.length > 16 ? `${repo.name.substring(0, 13)}...` : repo.name}
								subheader={new Date(repo.created_at).toLocaleDateString(
									undefined,
									{
										year: 'numeric',
										month: 'long',
										day: 'numeric',
									},
								)}
							/>
							<Link
								href={repo.trees_url}
								underline='none'
								onClick={(event) => getRepoDetail(event, repo)}>
								<CardMedia
									component='img'
									height='194'
									image={repo.owner.avatar_url}
									alt='Paella dish'
								/>
							</Link>
							<Link
								href={repo.trees_url}
								underline='none'
								onClick={(event) => getRepoDetail(event, repo)}>
								<CardContent>
									<Typography variant='body2' color='text.secondary'>
										{
											repo?.description?.substring(0, 30)
												?
												`${repo?.description?.substring(0, 30)}...`
												:
												'No description available'
										}
									</Typography>
								</CardContent>
							</Link>
							<CardActions disableSpacing>
								<IconButton aria-label='add to favorites'>
									<FavoriteIcon />
								</IconButton>
								<IconButton aria-label='share'>
									<ShareIcon />
								</IconButton>
							</CardActions>
						</Card>
					</Grid>
				))
			}
		</>
	);
}

export default SearchItems;
