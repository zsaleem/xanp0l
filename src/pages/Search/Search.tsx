import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import SearchItems from './components/SearchItems/';
import useFetcher from '../../common/hooks/useFetcher/';
import Title from '../../common/components/Title/';
import Loader from '../../common/components/Loader/';

const BASE_URL = process.env.REACT_APP_BASE_URL;

const Search: React.FC = () => {
	const params = useParams<any>();
	const URL = `${BASE_URL}users/${params.query}/repos?sort=date&order=desc`;
	const { loading, result, setQuery } = useFetcher(URL);

	return (
		<Container maxWidth='lg'>
			<Title text='Search result' />
			<Grid container spacing={2}>
				{
					loading ? (
						<Loader />
					) : (
						<SearchItems items={result} />
					)
				}
			</Grid>
		</Container>
	);
}

export default Search;
