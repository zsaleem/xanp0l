import Box from '@mui/material/Box';
import { makeStyles } from '@mui/styles';
import { styled } from '@mui/material/styles';

export const BoxContainer = styled(Box)({
	position: 'absolute',
	top:  '50%',
	left: '50%',
	width:  '50%',
	transform: 'translate(-50%, -50%)',
});

export const useStyles = makeStyles({
	small: {
		width: '95% !important',
	},
	normal: {},
});
