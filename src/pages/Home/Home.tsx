import React from 'react';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import useMediaQuery from '@mui/material/useMediaQuery';
import Title from '../../common/components/Title/';
import Input from '../../common/components/Input/';
import Button from '../../common/components/Button/';
import useSearch from '../../common/hooks/useSearch/';
import { BoxContainer, useStyles } from './styled';

const Home: React.FC = () => {
	const { loading, triggerSearch, errors } = useSearch();
	const small = useMediaQuery('(max-width:750px)');
	const classes = useStyles();
	
	return (
		<Container>
			<BoxContainer className={small ? `hello ${classes.small}` : `bye ${classes.normal}`}>
				<Title text='Get Repos List by Username' />
				<form onSubmit={triggerSearch}>
					<Input
						type='search'
						name='search'
	          id='outlined-error-helper-text'
	          label='Search by github username'
	          placeholder='e.g. zafar-saleem'
	          helperText={errors}
					/>
					<Button
						loading={loading}
						loadingPosition='end'
						variant='contained'
						type='submit'
						label='Search'
					/>
				</form>
			</BoxContainer>
		</Container>
	);
};

export default Home;
