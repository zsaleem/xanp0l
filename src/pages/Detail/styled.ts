import Box from '@mui/material/Box';
import { styled } from '@mui/material/styles';

export const BoxContainer = styled(Box)({
	pre: {
		overflow: 'auto',
	},
	code: {
		backgroundColor: '#f9f9f9',
	}
});
