import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import Container from '@mui/material/Container';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import FolderIcon from '@material-ui/icons/Folder';
import InsertDriveFileIcon from '@mui/icons-material/InsertDriveFile';
import Markdown from 'markdown-to-jsx';
import axios from 'axios';
import Title from '../../common/components/Title/';
import Loader from '../../common/components/Loader/';
import useFetcher from '../../common/hooks/useFetcher/';
import { BoxContainer } from './styled';

const BASE_URL = process.env.REACT_APP_BASE_URL;

const Detail: React.FC = () => {
	const [readmeFileUrl, setReadmeFileUrl] = useState<any>();
	const [md, setMd] = useState<any>('');
	const params = useParams<any>();
	const URL = `${BASE_URL}repos/${params.owner}/${params.repo}/git/trees/master?recursive=1`;
	const { loading, result, error } = useFetcher(URL);

	useEffect(() => {
		if (result) {
			result.tree.map((readmeFile: any) => {
				if (readmeFile.path === 'README.md') {
					setReadmeFileUrl(readmeFile.url);
				}
			})
		}
	}, [result]);

	useEffect(() => {
		async function fetchReadmeContents() {
			const response: any = await axios({
				url: readmeFileUrl,
			});

			const text: any = await response.data.content;
			if (response.status === 200) {
				setMd(atob(text));
			}
		}
		if (readmeFileUrl) {
			fetchReadmeContents();
		}
	}, [readmeFileUrl]);

	return (
		<Container>
			<Title text={params.repo} />
			<List>
				{
					loading ? (
						<Loader />
					) : (
						error ? (
							<p>{error}</p>
						) : (
							result?.tree.map((repoItem: any, repoIndex: number) => (
								repoItem.path.indexOf('/') > -1 ? (
									null
								) : (
									<ListItemButton key={`${repoItem}-${repoIndex}`}>
										<ListItem>
											<ListItemIcon>
												{
													repoItem.mode === '040000' ? (
														<FolderIcon />
													) : (
														<InsertDriveFileIcon />
													)
												}
											</ListItemIcon>
											<ListItemText
												primary={repoItem.path}
												secondary={repoItem.sha}
											/>
										</ListItem>
									</ListItemButton>
								)
							))
						)
					)
				}
			</List>
			<BoxContainer>
				<Markdown>{md}</Markdown>
			</BoxContainer>
		</Container>
	);
}

export default Detail;
