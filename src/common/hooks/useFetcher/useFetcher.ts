import { useState, useEffect } from 'react';
import axios from 'axios';

export default function useSearch(url: string) {
	const [loading, setLoading] = useState<boolean>(false);
	const [result, setResult] = useState<any>();
	const [query, setQuery] = useState<string>();
	const [newUrl, setUrl] = useState<string>();
	const [error, setError] = useState<string>();

	useEffect(() => {
		async function fetcher() {
			try {
				const response = await axios.get(url);

				if (response.status !== 200) {
					setError(response.statusText);
					setLoading(false);
					return;
				}
				setResult(response.data);
				setError('');
				setLoading(false);
			} catch(error: any) {
				setError(error.message);
				setLoading(false);
			}
		}
		setLoading(true);
		fetcher();
	}, [query, newUrl]);

	return { loading, result, setQuery, error };
}