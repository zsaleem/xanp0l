import { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';

const BASE_URL = process.env.REACT_APP_BASE_URL;

export default function useSearch() {
	const [loading, setLoading] = useState<boolean>(false);
	const [result, setResult] = useState<any>();
	const [query, setQuery] = useState<string>();
	const [errors, setError] = useState<string>();
	const history = useHistory<any>();

	useEffect(() => {
		async function fetcher() {
			const response: any = await axios.get(`${BASE_URL}users/${query}/repos`);
			if (response.status !== 200) {
				setLocalStates({
					error: response.statusText,
					loading: false,
					results: [],
				});
				return;
			}

			setStatesIfResponse(response);
		}
		if (query !== undefined && query?.length > 2) {
			fetcher();
		}
	}, [query]);

	const setStatesIfResponse = ({ data }: { data: any }) => {
		if (!data || data?.length === 0) {
			setLocalStates({
				error: 'No repos found',
				loading: false,
				results: [],
			});
			return false;
		}
		setLocalStates({
			error: '',
			loading: false,
			results: data,
		});
		history.push(`/search/${query}`);

		return true;
	};

	const setLocalStates = ({
		error,
		results,
		loading
	}: {
		error: string,
		results: any,
		loading: boolean,
	}) => {
		setError(error);
		setResult(results);
		setLoading(loading);
	};

	const triggerSearch = (event: any) => {
		event.preventDefault();
		if (!validate(event.target.search.value)) {
			return;
		}
		setLoading(true);
		setQuery(event.target.search.value || query);
		return false;
	};

	const validate = (value: string) => {
		if (!value) {
			setError('Please enter github username');
			return false;
		}
		setError('');
		return true;
	};

	return { loading, result, triggerSearch, errors };
}