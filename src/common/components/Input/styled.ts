import TextField from '@mui/material/TextField';
import { styled } from '@mui/material/styles';

export const InputStyled = styled(TextField)({
	width: '100%',
});