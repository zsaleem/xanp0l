import React from 'react';
import { InputStyled } from './styled';

interface InputProps {
	id?: string;
	label?: string;
	placeholder?: string;
	type?: string;
	name?: string;
	helperText?: string;
}

const Input: React.FC<InputProps> = ({
	id,
	label,
	placeholder,
	type,
	name,
	helperText,
}) => (
	<InputStyled
		error={helperText ? true : false}
		id={id}
		label={label}
		placeholder={placeholder}
		type={type}
		name={name}
		helperText={helperText}
	/>
);

export default Input;
