import React from 'react';
import Typography from '@mui/material/Typography';
import useMediaQuery from '@mui/material/useMediaQuery';

const Title: React.FC<{ text?: string}> = ({ text }) => {
	const small = useMediaQuery('(max-width:865px)');
	return (
		<Typography variant={small ? 'h5' : 'h4'} component='h1' gutterBottom>
			{text}
		</Typography>
	)
};

export default Title;
