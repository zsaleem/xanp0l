import React from 'react';
import SearchIcon from '@mui/icons-material/Search';
import { ButtonStyled }  from './styled';

interface ButtonProps {
	loading: boolean;
	loadingPosition: 'center' | 'end' | 'start';
	variant: 'text' | 'outlined' | 'contained';
	type: 'button' | 'submit' | 'reset';
	label: string;
}

const Button: React.FC<ButtonProps> = ({
	loading,
	loadingPosition,
	variant,
	type,
	label,
}) => {
	return (
		<ButtonStyled
			loading={loading}
			endIcon={<SearchIcon />}
			loadingPosition={loadingPosition}
			variant={variant}
			type={type}
		>
			{label}
		</ButtonStyled>
	);
};

export default Button;
