import LoadingButton from '@mui/lab/LoadingButton';
import { makeStyles } from '@mui/styles';
import { styled } from '@mui/material/styles';

export const ButtonStyled = styled(LoadingButton)({
	position: 'absolute',
	top: '120px',
	right: '0',
});
