import * as React from 'react';
import { Switch, BrowserRouter, Route } from 'react-router-dom';
import Home from './pages/Home/';
import Search from './pages/Search/';
import Detail from './pages/Detail/';

export default function App() {
	return (
		<BrowserRouter>
			<Switch>
				<Route path='/' exact>
					<Home />
				</Route>
				<Route path='/search/:query'>
					<Search />
				</Route>
				<Route path='/detail/:owner/:repo'>
					<Detail />
				</Route>
			</Switch>
		</BrowserRouter>
	);
}
