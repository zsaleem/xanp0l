## Requirements
Below is the stack I used in this project. Please follow the getting started section in order to make this project run.

## Stack
 - React
 - React Hooks
 - React Router DOM
 - JSX
 - Material UI
 - Axios
 - Github API
 - Material UI Media Queries
 - Mobile first responsive
 - ES6+
 - TypeScript
 - Git
 - Gitflow
 - Gitlab
 - Gitlab CI/CD
 - Heroku
 - NPM/Yarn
 - Sublime Text
 - Mac OS X
 - Google Chrome Incognito
 - eslint
 - nodejs(16.9.1)

## Getting started
In order to run this project in the browser successfully, please follow the steps below!

    1. Clone this repository.
    2. cd into `xanpool` directory.
    3. Run `yarn/npm install` command to download and install all dependencies.
    4. To run this project use `yarn/npm start:dev` command in command line.
    5. To build the project for production run `yarn/npm build` command. This will build the app for production and put all the files in `/build` folder.

## Live Demo
To view live demo please [click here](https://xanpool.herokuapp.com/).

To view a screencast view [click here](https://youtu.be/czUgDwVIejM).

To view list of CD/CD pipeline [click here](https://gitlab.com/zsaleem/xanp0l/-/pipelines).

To view list of commits [click here](https://gitlab.com/zsaleem/xanp0l/-/commits/master).

To view list of all branches [click here](https://gitlab.com/zsaleem/xanp0l/-/branches/active).

### For more details and documentation, I recommend to view the [wiki](https://gitlab.com/zsaleem/xanp0l/-/wikis/home) page.